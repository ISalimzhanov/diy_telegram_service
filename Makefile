PYTHON := python3
DOCKER_IMAGE_TEST := 41694/diy_telegram_microservice:test
DOCKER_IMAGE := 41694/diy_telegram_microservice:latest

.PHONY: test
test:
	poetry run pytest

.PHONY: install
install:
	$(PYTHON) -m pip install wheel setuptools
	poetry export -f requirements.txt --output requirements.txt --without-hashes
	$(PYTHON) -m pip install -r requirements.txt

.PHONY: lint
lint:
	poetry run pre-commit run --all-files

.PHONY: docker-push
docker-push:
	docker build -t $(DOCKER_IMAGE) .
	docker push $(DOCKER_IMAGE)

.PHONY: docker-push-test
docker-push-test:
	docker build -t $(DOCKER_IMAGE_TEST) .
	docker push $(DOCKER_IMAGE_TEST)


.PHOMY: ci-install
ci-install:
	poetry install --no-dev
