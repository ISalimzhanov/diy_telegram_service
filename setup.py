from setuptools import find_packages, setup

with open("README.md") as f:
    readme = f.read()

with open("LICENSE") as f:
    license = f.read()

setup(
    name="Telegram bot for authentication",
    version="0.1.0",
    description="Telegram bot for authentication",
    long_description=readme,
    author="Iskander Salimzhanov",
    url="https://gitlab.com/ISalimzhanov/diy_telegram_service",
    license=license,
    packages=find_packages(exclude=("tests", "docs")),
)
