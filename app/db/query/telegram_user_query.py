from typing import Optional

from sqlalchemy import select

from ..models import TelegramUserModel
from ..session_config import SessionConfig


class TelegramUserQuery:
    def __init__(self, session_config: SessionConfig):
        self.session_config = session_config

    def find_by_alias(self, alias: str) -> Optional[TelegramUserModel]:
        statement = select(TelegramUserModel).filter_by(alias=alias)
        telegram_user = self.session_config.session.execute(statement).first()
        if telegram_user is not None:
            return telegram_user[0]
        return None

    def save(self, telegram_user: TelegramUserModel):
        self.session_config.session.merge(telegram_user)
        self.session_config.session.commit()
