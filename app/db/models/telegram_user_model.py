from sqlalchemy import Column, VARCHAR, Integer

from ..session_config import Base


class TelegramUserModel(Base):
    __tablename__ = "telegram_user"

    alias = Column(VARCHAR(36), nullable=False, primary_key=True)
    chat_id = Column(Integer, nullable=False, unique=True)
