from sqlalchemy import create_engine
from sqlalchemy.orm import Session, declarative_base

Base = declarative_base()


class SessionConfig:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not SessionConfig._instance:
            SessionConfig._instance = super(SessionConfig, cls).__new__(cls)
        return SessionConfig._instance

    def __init__(self, user: str, password: str, db_name: str, host: str, port: int):
        self.__engine = create_engine(
            f"mariadb+pymysql://{user}:{password}@{host}:{port}/{db_name}?charset=utf8mb4")
        self.session = Session(self.__engine)

    def create_tables(self):
        Base.metadata.create_all(self.__engine)
