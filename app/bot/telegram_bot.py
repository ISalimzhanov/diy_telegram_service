from os import environ

import telebot
from telebot.types import *

from .abstract_bot import AbstractBot
from ..resources.strings import *
from ..service import TelegramService


class TelegramBot(AbstractBot):
    def __init__(self, telegram_service: TelegramService):
        self.telegram_service = telegram_service
        tg_api_caller = telebot.TeleBot(token=environ['TELEGRAM_BOT_TOKEN'])
        super(TelegramBot, self).__init__(messenger_api_caller=tg_api_caller)

    def greet(self, message: Message):
        self.api_caller.send_message(chat_id=message.chat.id, text=GREET_MSG)
        self.api_caller.register_next_step_handler_by_chat_id(message.chat.id, self.help_menu)
        self.telegram_service.save_telegram_user(message.from_user.username, message.chat.id)

    def help_menu(self, message: Message):
        self.api_caller.send_message(chat_id=message.chat.id, text=HELP_MSG)
        self.api_caller.register_next_step_handler(message, self.help_menu)

    def send_token(self, alias: str, token: str):
        chat_id = self.telegram_service.find_chat_id_by_alias(alias)
        self.api_caller.send_message(chat_id=chat_id, text=TOKEN_MSG.format(token))
        self.api_caller.register_next_step_handler_by_chat_id(chat_id, self.help_menu)

    def run(self):
        @self.api_caller.message_handler(commands=['start'])
        def greet(message: Message):
            self.greet(message)

        @self.api_caller.message_handler(commands=['help'])
        def help(message: Message):
            self.help_menu(message.chat.id)

        while True:
            try:
                self.api_caller.polling(none_stop=True)
            except ConnectionError:
                continue
