from abc import ABC, abstractmethod


class AbstractBot(ABC):
    _instance = None

    def __init__(self, messenger_api_caller):
        self.api_caller = messenger_api_caller

    @abstractmethod
    def greet(self, **kwargs):
        pass

    @abstractmethod
    def help_menu(self, **kwargs):
        pass

    @abstractmethod
    def send_token(self, **kwargs):
        pass

    @abstractmethod
    def run(self, **kwargs):
        pass
