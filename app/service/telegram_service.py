from ..db import SessionConfig, TelegramUserModel
from ..db.query import TelegramUserQuery
from ..exception.alias_not_found_error import AliasNotFoundError


class TelegramService:
    def __init__(self, session_config: SessionConfig):
        self.telegram_user_query = TelegramUserQuery(session_config)

    def find_chat_id_by_alias(self, alias: str) -> int:
        telegram_user = self.telegram_user_query.find_by_alias(alias)
        if telegram_user is None:
            raise AliasNotFoundError("bot doesn't know user with such alias")
        return telegram_user.chat_id

    def save_telegram_user(self, alias: str, chat_id: int) -> None:
        if self.telegram_user_query.find_by_alias(alias) is None:
            telegram_user = TelegramUserModel()
            telegram_user.chat_id = chat_id
            telegram_user.alias = alias
            self.telegram_user_query.save(telegram_user)
