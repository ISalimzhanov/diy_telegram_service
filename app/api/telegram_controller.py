from flask import Response, request, make_response
from flask_cors import cross_origin

from ..api.app import app, ROOT_URL, telegram_bot


@app.route(f"{ROOT_URL}/prediction/grade")
@cross_origin(supports_credentials=True)
def send_token() -> Response:
    alias = request.args.get("alias")
    token = request.args.get("token")

    # toDo validating
    bad_request_msg = "Some request parameters are not provided:"  # toDo move to util package
    if alias is None:
        bad_request_msg += "alias,"
    if token is None:
        bad_request_msg += "token,"
    bad_request_msg = bad_request_msg[:-1]
    if alias is None:
        return make_response(bad_request_msg, 400)
    telegram_bot.send_token(alias, token)
    return make_response("ok", 200)
