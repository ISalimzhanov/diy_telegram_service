import os
from threading import Thread

from dotenv import load_dotenv
from flask import Flask
from flask_cors import CORS

from ..bot import TelegramBot
from ..db import SessionConfig
from ..service import TelegramService

load_dotenv()
ROOT_URL = "/api"
session_config = SessionConfig(
    user=os.environ["DB_USER"],
    password=os.environ["DB_PASSWORD"],
    db_name=os.environ["DB_NAME"],
    host=os.environ["DB_HOST"],
    port=int(os.environ["DB_PORT"]),
)
session_config.create_tables()

telegram_service = TelegramService(session_config=session_config)

telegram_bot = TelegramBot(telegram_service)
thread = Thread(target=telegram_bot.run)
thread.run()

app = Flask(__name__)
CORS(app, support_credentials=True)
